@extends('layouts.template')

@section('content')

<div class="col-md-8">
	<div class="py-3">
		<a href="/categoryadd" class="btn btn-outline-primary float-right">ADD</a>
	</div><br>

	<table class="table table-secondary table-hover table-bordered">
		<thead>
		<th>
			<tr>
				<td>No.</td>
				<td>Name</td>

			</tr>
		</th>
		</thead>
		@foreach($categories as $category)
		<tbody>
			<tr>
				<td>
					{{$category->id}}
				</td>
				<td>
					{{$category->category_name}}
				</td>
			</tr>
		</tbody>
			@endforeach
	</table>

</div>

@endsection