@extends('layouts.template')

@section('content')

<div class="col-md-8">
	@if(count($errors))
	<div class="alert alert-danger">
		<ul>@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<form method="post" action="{{URL :: To('post/edit')}}" enctype="multipart/form-data" class="my-3">
		@csrf
		<div class="form-group">
			<label>Post Title:</label>
			<input type="text" name="title" class="form-control">
			
		</div>

		<div class="form-group">
			<label>Photo:</label>
			<input type="file" name="photo" class="form-control-file">
			
		</div>
		
		<div class="form-group">
			<option value="">Choose Category Name</option>
			@foreach($categories as $category)
			<option value="{{$category->id}}" @if($category->id==$post->category_id)
			{{selected}}
			@endif>{{$category->category_name}}
		</option>
			@endforeach
		
		
		</div>

		<div class="form-group">
			<label>Post Body:</label>
			<textarea name="body" class="form-control" id="summernote"></textarea>
		</div>

		

	</form>
</div>

@endsection
